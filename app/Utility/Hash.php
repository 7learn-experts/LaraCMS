<?php


namespace App\Utility;


class Hash
{
    public static function token($length = 5)
    {
        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }

        return mt_rand(10000, 999999);

    }
}